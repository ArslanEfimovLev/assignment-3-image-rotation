
#include "bmp.h"
#include "rotations.h"
#include "validation.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#define ERROR_CODE_RETURN 1
#define OK_CODE_RETURN 0

void print_error(const char* s){
    fprintf(stderr, "%s\n", s);
    exit(ERROR_CODE_RETURN);
}

void print_success(const char* s){
    fprintf(stdout, "%s\n", s);
    exit(OK_CODE_RETURN);
}

int main( int argc, char** argv ) {

    if(validate_count_data_input(argc) == VALIDATION_ERROR){
        print_error("Incorrect data entry, example: <source-image> <transformed-image> <angle>");
    }
    const char* source_image = argv[1];
    const char* transformed_image = argv[2];
    const int angle = atoi(argv[3]); //to int

    if(validate_angle(angle)!=VALIDATION_OK){
        print_error("Angle should be 0 or -90 or 90 or -180 or 180 or -270 or 270");
    }
    struct image source_img_struct = {0};
    FILE* input_image = fopen(source_image, "rb");

    if(input_image == NULL){
        print_error("Not find input file");
    }

    if(access(source_image, R_OK) == -1){
        print_error("No read access to the source image file");
    }


    enum read_status status_read = from_bmp(input_image, &source_img_struct);
    fclose(input_image);
    if(status_read!=READ_OK){
        fclose(input_image);
        print_error("Problem with reading input file");
    }

    FILE* output_image = fopen(transformed_image, "wb");

    if(access(transformed_image, W_OK) == -1){
        fclose(output_image);
        free(source_img_struct.data);
        print_error("No write access to the transformed image file");
    }

    const uint32_t rotations = get_rotations(angle);
    source_img_struct = set_image_like_rotated(rotations, &source_img_struct);
    if(source_img_struct.data==NULL){
        print_error("Failed to rotate the image");
    }
        enum write_status status_write = to_bmp(output_image, &source_img_struct);
    if(status_write != WRITE_OK){
        free(source_img_struct.data);
        fclose(output_image);
        print_error("Problem with writing output file");
    }

    free(source_img_struct.data);
    fclose(output_image);
    print_success("The Image rotated successfully!");


}
