//
// Created by Арслан Ефимов on 08.11.2023.
//
#include "bmp.h"
#include <stdio.h>
#include <stdlib.h>
#ifndef BMP_IO_C
#define BMP_IO_C
#define bFTYPE 0x4D42
#define BiSize 40
#define BiPlanes 1
#define BITMAP_24BIT_RGB 24


//read header
enum read_status read_bmp_header(FILE* input_file, struct bmp_header* bmpHeader){
    size_t count_read_header = fread(bmpHeader, sizeof(struct bmp_header), 1, input_file);
    if(count_read_header!=1){
        return READ_INVALID_HEADER;
    }
    if(bmpHeader->bfType != bFTYPE){
        return READ_INVALID_SIGNATURE;
    }

    if(bmpHeader->biBitCount != BITMAP_24BIT_RGB){
        fprintf(stderr, "%s/n", "our program only supports 24-bit bmp format");
        return INVALID_FORMAT;
    }

    return READ_OK;
}

// calculate padding if image width % 4 != 0
inline uint32_t calculate_padding_row_size(struct image const* img){
    uint32_t row_count_pixels = img->width * sizeof(struct pixel);
    return (4-(row_count_pixels % 4)) % 4;
}

struct bmp_header create_header(struct image const* img){
    struct bmp_header bmpHeader = {0};
    uint32_t row_padding = calculate_padding_row_size(img);
    uint32_t bmp_Image_Size = (row_padding + img->width * sizeof(struct pixel))*img->height;
    uint32_t bmp_file_size = (row_padding + img->width * sizeof(struct pixel)) * img->height + sizeof(struct bmp_header);
    bmpHeader.bfType = bFTYPE;
    bmpHeader.bfileSize = bmp_file_size;
    bmpHeader.bfReserved = 0;
    bmpHeader.bOffBits = sizeof(struct bmp_header);
    bmpHeader.biSize = BiSize;
    bmpHeader.biWidth = img->width;
    bmpHeader.biHeight = img->height;
    bmpHeader.biPlanes = BiPlanes;
    bmpHeader.biBitCount = BITMAP_24BIT_RGB;
    bmpHeader.biCompression = 0;
    bmpHeader.biSizeImage = bmp_Image_Size;
    bmpHeader.biXPelsPerMeter = 0;
    bmpHeader.biYPelsPerMeter = 0;
    bmpHeader.biClrUsed = 0;
    bmpHeader.biClrImportant = 0;
    return bmpHeader;


}

enum read_status from_bmp(FILE* input_file, struct image* img){

    struct bmp_header bmpHeader = {0};
    enum read_status return_status = read_bmp_header(input_file, &bmpHeader);
    if(return_status != READ_OK){
        return return_status;
    }
    *img = (set_image(bmpHeader.biWidth, bmpHeader.biHeight));
    if(!img->data){
        return READ_INVALID_BITS;
    }
    const uint32_t row_padding = calculate_padding_row_size(img);

    for (uint32_t number_row = 0; number_row < img->height; number_row++) {
        uint32_t pos_pixel = number_row * img->width; //pos pixel
        if (fread(&img->data[pos_pixel], sizeof(struct pixel), img->width, input_file) != img->width) {
            free(img->data);
            return READ_INVALID_IMAGE; // Error reading image data
        }
        // Skip padding'а
        if(fseek(input_file, (long ) row_padding, SEEK_CUR) != 0){
            free(img->data);
            return INVALID_SKIP_BITS;
        }
    }

    return READ_OK;

}

enum write_status to_bmp(FILE* output_file, struct image const* img) {
    struct bmp_header bmpHeader = create_header(img);
    uint32_t row_padding = calculate_padding_row_size(img);

    //trying to write header to a file
    if (fwrite(&bmpHeader, sizeof(struct bmp_header), 1, output_file) != 1) {
        return WRITE_ERROR;
    }
    //trying to write img to a file
    for (uint32_t row_number = 0; row_number < img->height; row_number++) {
        if (fwrite(img->data + (row_number * img->width), sizeof(struct pixel), img->width, output_file) != img->width) {
            return WRITE_ERROR;
        }


        uint8_t padding_byte = 0;
        // trying to write padding to file
        for (uint32_t padd_b = 0; padd_b < (long ) row_padding; padd_b++) {
            if (fwrite(&padding_byte, sizeof(uint8_t), 1, output_file) != 1) {
                return WRITE_PADDING_BYTE_ERROR;
            }

        }
    }
    return WRITE_OK;
}

#endif
