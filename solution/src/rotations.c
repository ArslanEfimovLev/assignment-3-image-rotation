//
// Created by Арслан Ефимов on 09.11.2023.
//
#include "image.h"
#include "rotations.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#ifndef ROTATIONS_C
#define ROTATIONS_C
#define FULL_ANGLE 360
#define STRAIGHT_ANGLE_FOR_COUNT_ROTATIONS 90
#define ROTATION_COUNT_NUMBER 4


//Counts the number of turns by 90 degrees, taking into account negative hums
uint32_t get_rotations(int angle){
    int rotations = ((angle + FULL_ANGLE) % FULL_ANGLE) / STRAIGHT_ANGLE_FOR_COUNT_ROTATIONS;
    // Set the number of turns to the range from 0 to 3
    rotations = ( ROTATION_COUNT_NUMBER - rotations ) % ROTATION_COUNT_NUMBER;
    return rotations;
}


static struct image rotate(struct image const source) {
    struct image rotated;
    rotated = set_image(source.height, source.width); //Reverse the order of height and width for rotation
    if(!rotated.data){
        free(rotated.data);
        fprintf(stderr, "%s\n", "malloc returned NULL, failed to allocate memory on the heap");
        return (struct image){.height=0, .width=0, .data=NULL};
    }
        for (uint32_t row_number = 0; row_number < source.height; row_number++) {
            for (uint32_t column_number = 0; column_number < source.width; column_number++) {
                uint32_t our_place_pixel = row_number * source.width + column_number;
                uint32_t rotated_place_pixel = column_number * rotated.width + (rotated.width - 1 - row_number);
                rotated.data[rotated_place_pixel] = source.data[our_place_pixel];

            }
        }

    free(source.data); //we free the image, since after the rotate function it is no longer needed
    return rotated;
}

struct image set_image_like_rotated(uint32_t rotations,  struct image const* source_img_struct){
    struct image rotated = *source_img_struct; //create image struct for rotate
    for(uint32_t r = 0; r < rotations; r++){
        rotated = rotate(rotated);

    }
    return rotated;
}

#endif
