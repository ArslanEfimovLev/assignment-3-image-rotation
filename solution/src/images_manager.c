//
// Created by Арслан Ефимов on 11.11.2023.
//
#include "image.h"
#include <stdio.h>
#include <stdlib.h>
#ifndef IMAGE_C
#define IMAGE_C

// set image structure
struct image set_image(const uint64_t width, const uint64_t height){
    struct image img = {0};
    img.width = width;
    img.height = height;
    img.data = malloc(sizeof(struct pixel) * img.width * img.height);
    if(!img.data){
        img.width = 0;
        img.height = 0;
        fprintf(stderr, "%s\n", "malloc returned NULL, failed to allocate memory on the heap");

    }
    return img;
}
#endif
