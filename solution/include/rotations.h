#include "image.h"
#include <stdint.h>
#include <stdlib.h>
#ifndef ROTATIONS_H
#define ROTATIONS_H

uint32_t get_rotations(int angle);
struct image set_image_like_rotated(uint32_t rotations, struct image const* source_img_struct);

#endif
