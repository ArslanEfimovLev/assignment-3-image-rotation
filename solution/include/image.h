
#include <stdint.h>
#ifndef IMAGE_H
#define IMAGE_H
struct image {
    uint64_t width, height;
    struct pixel* data;
};
struct __attribute__((packed)) pixel{
        uint8_t b, g, r;
    };

//Utils
struct image set_image(uint64_t width, uint64_t height);

#endif
