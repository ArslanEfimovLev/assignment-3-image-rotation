#include "image.h"
#include  <stdint.h>
#include <stdio.h>
#ifndef BMP_IO_H
#define BMP_IO_H
struct  __attribute__((packed)) bmp_header{
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};

/*  deserializer   */
enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    INVALID_FORMAT,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_INVALID_IMAGE,
    INVALID_SKIP_BITS,
    /* коды других ошибок  */
};

enum read_status from_bmp(FILE* input_file, struct image* img );

/*  serializer   */
enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR,
    WRITE_PADDING_BYTE_ERROR
    /* коды других ошибок  */
};

enum write_status to_bmp(FILE* output_file, struct image const* img );
uint32_t calculate_padding_row_size(struct image const* img);
#endif
